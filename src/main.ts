import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

const { HOST = "127.0.0.1", PORT = 3050 } = process.env;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({
    transform: true,
    transformOptions: {
      enableImplicitConversion: true
    }
  }));
  console.log(`Published on http://${HOST}:${PORT}`);
  await app.listen(PORT, HOST);
}
bootstrap();
