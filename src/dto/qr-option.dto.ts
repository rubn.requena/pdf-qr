import { Transform, Type } from "class-transformer"
import { IsInt, IsNotEmpty, IsNumber, IsOptional } from "class-validator"

export class QROptions {
  @IsNotEmpty()
  payload: string

  @IsInt()
  @Type(() => Number)
  x: number

  @IsInt()
  @Type(() => Number)
  y: number

  @IsInt()
  @Type(() => Number)
  page: number

  @IsNumber()
  @Type(() => Number)
  scale: number
}